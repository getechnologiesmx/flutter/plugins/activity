# ActivityPlugin

[![Getechnologies: Activity](https://i.ibb.co/XsC7KYB/logoGet.png)](https://gitlab.com/getechnologiesmx/flutter/plugins/activity.git)

Launch and get response other apps,use intents when the platform is Android.

## Usage

### Minimal example startActivityForResult

```dart
if (platform.isAndroid) {
const intent = AndroidIntent(action: 'getmx.PAY',arguments: {'amount': 90,'currency': 'MXN'});
Map<String,dynamic>? map = await activityPlugin.startActivityForResult(intent);
}
```

