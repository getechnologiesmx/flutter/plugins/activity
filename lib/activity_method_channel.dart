import 'package:activity_getmx/android_intent.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'activity_platform_interface.dart';

class MethodChannelActivity extends ActivityPlatform {
  @visibleForTesting
  final methodChannel =
      const MethodChannel('com.getechnologiesmx.flutter.activity');

  @override
  Future<Map<Object?, Object?>?> startActivityForResult(
      AndroidIntent? intent) async {
    Map<String, dynamic>? map = intent?.buildArguments();
    return await methodChannel.invokeMethod<Map<Object?, Object?>?>(
        'startActivityForResult', map);
  }
}
