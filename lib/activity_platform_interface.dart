import 'package:activity_getmx/android_intent.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'activity_method_channel.dart';

abstract class ActivityPlatform extends PlatformInterface {
  /// Constructs a ActivityPlatform.
  ActivityPlatform() : super(token: _token);

  static final Object _token = Object();

  static ActivityPlatform _instance = MethodChannelActivity();

  /// The default instance of [ActivityPlatform] to use.
  ///
  /// Defaults to [MethodChannelActivity].
  static ActivityPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [ActivityPlatform] when
  /// they register themselves.
  static set instance(ActivityPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<Map<Object?, Object?>?> startActivityForResult(AndroidIntent? intent) {
    throw UnimplementedError(
        'startActivityForResult() has not been implemented.');
  }
}
