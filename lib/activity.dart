
import 'package:activity_getmx/android_intent.dart';

import 'activity_platform_interface.dart';

class Activity {
  Future<Map<String, dynamic>?> startActivityForResult(
      AndroidIntent intent) async {
    Map<Object?, Object?>? map =
        await ActivityPlatform.instance.startActivityForResult(intent);
    return convertirMapa(map);
  }

  Map<String, dynamic> convertirMapa(Map<Object?, Object?>? mapaOriginal) {
    Map<String, dynamic> nuevoMapa = {};

    mapaOriginal?.forEach((key, value) {
      nuevoMapa[key.toString()] = value;
    });

    return nuevoMapa;
  }
}
