import 'package:meta/meta.dart';

class AndroidIntent {
  const AndroidIntent({
    this.action,
    this.flags,
    this.category,
    this.data,
    this.arguments,
    this.arrayArguments,
    this.package,
    this.componentName,
    this.type,
  });

  @visibleForTesting
  AndroidIntent.private({
    this.action,
    this.flags,
    this.category,
    this.data,
    this.arguments,
    this.arrayArguments,
    this.package,
    this.componentName,
    this.type,
  });

  final String? action;
  final List<int>? flags;
  final String? category;
  final String? data;
  final Map<String, dynamic>? arguments;
  final Map<String, List<dynamic>>? arrayArguments;
  final String? package;
  final String? componentName;
  final String? type;

  bool _isPowerOfTwo(int x) {
    /* First x in the below expression is for the case when x is 0 */
    return x != 0 && ((x & (x - 1)) == 0);
  }

  @visibleForTesting
  int convertFlags(List<int> flags) {
    var finalValue = 0;
    for (var i = 0; i < flags.length; i++) {
      if (!_isPowerOfTwo(flags[i])) {
        throw ArgumentError.value(flags[i], 'flag\'s value must be power of 2');
      }
      finalValue |= flags[i];
    }
    return finalValue;
  }

  /// Constructs the map of arguments which is passed to the plugin.
  Map<String, dynamic> buildArguments() {
    return {
      if (action != null) 'action': action,
      if (flags != null) 'flags': convertFlags(flags!),
      if (category != null) 'category': category,
      if (data != null) 'data': data,
      if (arguments != null) 'arguments': arguments,
      if (arrayArguments != null) 'arrayArguments': arrayArguments,
      if (package != null) ...{
        'package': package,
        if (componentName != null) 'componentName': componentName,
      },
      if (type != null) 'type': type,
    };
  }
}
