import 'dart:async';
import 'package:activity/activity.dart';
import 'package:activity/android_intent.dart';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyAppState(),
    );
  }
}

class MyAppState extends StatelessWidget {
  final activityPlugin = Activity();

  Future<void> _fetchData(BuildContext context) async {
    try {
      const intent = AndroidIntent(
          action: 'getmx.PAY', arguments: {'amount': 90, 'currency': 'MXN'});
      Map<String, dynamic>? map =
          await activityPlugin.startActivityForResult(intent);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Data'),
            content: Text('Response: $map'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop(); // Cierra el AlertDialog
                },
                child: Text('Ok'),
              ),
            ],
          );
        },
      );
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Test Intent'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            _fetchData(context);
          },
          child: Text('Test Intent'),
        ),
      ),
    );
  }
}
