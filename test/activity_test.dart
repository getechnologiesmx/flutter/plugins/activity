import 'package:activity_getmx/activity_platform_interface.dart';
import 'package:activity_getmx/android_intent.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockActivityPlatform
    with MockPlatformInterfaceMixin
    implements ActivityPlatform {
  @override
  Future<Map<Object?, Object?>> startActivityForResult(AndroidIntent? map) {
    // TODO: implement startActivityForResult
    throw UnimplementedError();
  }
}

void main() {
  //nothing
}
