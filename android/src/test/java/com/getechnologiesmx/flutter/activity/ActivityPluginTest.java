package com.getechnologiesmx.flutter.activity;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import android.content.Intent;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * This demonstrates a simple unit test of the Java portion of this plugin's implementation.
 * <p>
 * Once you have built the plugin's example app, you can run these tests from the command
 * line by running `./gradlew testDebugUnitTest` in the `example/android/` directory, or
 * you can run them directly from IDEs that support JUnit such as Android Studio.
 */

public class ActivityPluginTest {
    @Test
    public void onMethodCall_getPlatformVersion_returnsExpectedValue() {
        ActivityPlugin plugin = new ActivityPlugin();

        final MethodCall call = new MethodCall("getPlatformVersion", null);
        MethodChannel.Result mockResult = mock(MethodChannel.Result.class);
        plugin.onMethodCall(call, mockResult);

        verify(mockResult).success("Android " + android.os.Build.VERSION.RELEASE);
    }


    @Test
    public void payment() {
        ActivityPlugin plugin = new ActivityPlugin();
        Intent intent = new Intent("getmx.PAY");
        intent.putExtra("amount", 10);
        intent.putExtra("currency", "MXN");

        Map<String, Object> map = new HashMap<>();
        map.put("action", "getmx.PAY");
        Map<String, Object> extra = new HashMap<>();
        extra.put("mount", 10);
        extra.put("currency", "MXN");
        map.put("extras", extra);
        final MethodCall call = new MethodCall("startActivityForResult", map);
        MethodChannel.Result mockResult = mock(MethodChannel.Result.class);
        plugin.onMethodCall(call, mockResult);
        System.out.println(mockResult.toString());
    }
}
